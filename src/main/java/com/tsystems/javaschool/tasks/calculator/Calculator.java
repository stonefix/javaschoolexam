package com.tsystems.javaschool.tasks.calculator;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.LinkedList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
     public String evaluate(String statement) {
        try {
            LinkedList<Double> digits = new LinkedList<>();
            LinkedList<Character> op = new LinkedList<Character>(); 
            DecimalFormat df = new DecimalFormat("##.####"); 
            DecimalFormatSymbols sym = DecimalFormatSymbols.getInstance();
            sym.setDecimalSeparator('.');
            df.setDecimalFormatSymbols(sym);
            for (int i = 0; i < statement.length(); i++) { 
                char c = statement.charAt(i);
                if (isIndent(c))
                    continue;
                if (c == '(')
                    op.add('(');
                else if (c == ')') {
                    while (op.getLast() != '(')
                        processOperator(digits, op.removeLast());
                    op.removeLast();
                } else if (isOperator(c)) {
                    while (!op.isEmpty() && prioritize(op.getLast()) >= prioritize(c))
                        processOperator(digits, op.removeLast());
                    op.add(c);
                } else {
                    String operand = "";
                    while (i < statement.length() && Character.isDigit(statement.charAt(i)) | statement.charAt(i) == '.') {
                        operand += statement.charAt(i++);
                    }
                    i--;
                    digits.add(Double.parseDouble(operand));
                }
            }
            while (!op.isEmpty())
                processOperator(digits, op.removeLast());
            return df.format(digits.get(0));
        } catch (Exception ex) {
            return null;
        }
    }

    static boolean isIndent(char c) { 
        return c == ' ';
    }

    static boolean isOperator(char c) { 
        return c == '+' || c == '-' || c == '*' || c == '/' ;
    }

    static int prioritize(char op) {
        switch (op) { 
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }

    static void processOperator(LinkedList<Double> digits, char operation) {
        double first = digits.removeLast(); 
        double second = digits.removeLast();
        switch (operation) {
            case '+':
                digits.add(second + first);
                break;
            case '-':
                digits.add(second - first);
                break;
            case '*':
                digits.add(second * first);
                break;
            case '/':
                if(first == 0){
                    digits.add(null);
                } else {
                    digits.add(second / first);
                }
                break;
        }
    }  

}
