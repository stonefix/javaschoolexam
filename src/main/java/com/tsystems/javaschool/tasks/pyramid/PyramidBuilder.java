package com.tsystems.javaschool.tasks.pyramid;


import java.util.List;
import java.util.Collections;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        int counter = 0;
        int lineCount;
        int columnCount;
        
        if ((inputNumbers.contains(null))||
            (Math.sqrt(inputNumbers.size()*8+1) - (int)Math.sqrt(inputNumbers.size()*8+1)!=0) ||
             inputNumbers.isEmpty()){
            throw  new CannotBuildPyramidException();
        }
        
        Collections.sort(inputNumbers);

        lineCount = (int) ((-1+Math.sqrt(1+8*inputNumbers.size()))/2);
        columnCount = lineCount*2 - 1;

        int[][] pyramidArray = new int[lineCount][columnCount];
        

        for(int i=0; i<lineCount;i++){
            for(int j = 0; j<columnCount;j++){
                if (((double)((i + j -(lineCount-1))%2)==0) && (i + j - (lineCount-1) >=0)
                        && (i + lineCount >= j)){
                    pyramidArray[i][j] = inputNumbers.get(counter);
                    counter++;
                }
            }
        }

        return pyramidArray;
        
    }


}
